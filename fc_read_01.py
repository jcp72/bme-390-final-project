from FlowCytometryTools import FCPlate, FCMeasurement
from FlowCytometryTools import PolyGate

viable_gate = PolyGate([
        (3.823e+05, 2.786e+05), (2.635e+05, 2.077e+05), 
        (1.991e+05, 1.481e+05), (2.132e+05, 8.284e+04), 
        (3.259e+05, 5.731e+04), (4.688e+05, 8.568e+04), 
        (6.299e+05, 1.509e+05), (8.191e+05, 2.446e+05), 
        (8.634e+05, 3.155e+05), (8.271e+05, 3.949e+05), 
        (7.446e+05, 4.091e+05), (6.178e+05, 3.694e+05), 
        (5.010e+05, 3.325e+05)
    ], ('FSC-H', 'SSC-H'), region='in', name='viable_gate')

singlets_gate = PolyGate([
        (1.070e+05, 2.194e+05), (8.556e+05, 8.387e+05), 
        (1.006e+06, 8.208e+05), (2.979e+05, 2.122e+05)
    ], ('FSC-A', 'FSC-H'), region='in', name='singlets_gate')

import matplotlib.pyplot as plt
import numpy as np
from statistics import median
from math import log10
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = ["Times New Roman"]


def heatmapify(x, y, nx, ny, do_log = True):

    if do_log:
        x = np.log10(x)
        y = np.log10(y)

    x_min = min(x)
    x_max = max(x)
    y_min = min(y)
    y_max = max(y)

    x_div = (x_max - x_min + 0.001) / nx # Note: +0.001 prevents index error 
    y_div = (y_max - y_min + 0.001) / ny # later in floor division

    hist_2d = np.zeros((nx, ny))

    # plt.scatter(x, (x - x_min) // x_div, s=4)
    # plt.show()

    def get_indices(xj, yj):
        x_index = int((xj - x_min) // x_div)
        y_index = int((yj - y_min) // y_div)
        return x_index, y_index

    for xi, yi in zip(x, y):
        x_index, y_index = get_indices(xi, yi)
        hist_2d[x_index][y_index] += 1
    
    result = np.zeros(len(x))

    for i, (xi, yi) in enumerate(zip(x, y)):
        x_index, y_index = get_indices(xi, yi)
        result[i] = hist_2d[x_index][y_index]
        
    return result

ROOT = "data-from-lauren/"

plate_info = {
    "A1": ["1.1", "Peptide 1 (1)", "2.1 1.fcs"],
    "B1": ["1.2", "Peptide 1 (2)", "2.1 2.fcs"],
    "A2": ["4.1", "Peptide 4 (1)", "2.4 1.fcs"],
    "B2": ["4.2", "Peptide 4 (2)", "2.4 2.fcs"],
    "A3": ["6.1", "Peptide 6 (1)", "2.6 1.fcs"],
    "B3": ["6.2", "Peptide 6 (2)", "2.6 2.fcs"],
    "A4": ["8.1", "Peptide 8 (1)", "2.8 1.fcs"],
    "B4": ["8.2", "Peptide 8 (2)", "2.8 2.fcs"],
    "C1": ["10.1", "Peptide 10 (1)", "2.10 1.fcs"],
    "D1": ["10.2", "Peptide 10 (2)", "2.10 2.fcs"],
    "C2": ["11.1", "Peptide 11 (1)", "2.11 1.fcs"],
    "D2": ["11.2", "Peptide 11 (2)", "2.11 2.fcs"],
    "C3": ["12.1", "Peptide 12 (1)", "2.12 1.fcs"],
    "D3": ["12.2", "Peptide 12 (2)", "2.12 2.fcs"],
    "E1": ["Blank.1", "Blank (no AMHR2, no uAb) (1)", "Copy of Blank 3.fcs"],
    "F1": ["Blank.2", "Blank (no AMHR2, no uAb) (2)", "Copy of Blank 4.fcs"],
    "E2": ["Pos.1", "Positive control (AMHR2, uAb) (1)", "Copy of AMH uAb_AMHR2 1.fcs"],
    "F2": ["Pos.2", "Positive control (AMHR2, uAb) (2)", "Copy of AMH uAb_AMHR2 2.fcs"],
    "E3": ["NegN.1", "Negative control (AMHR2, no uAb) (1)", "Copy of AMHR2 2.fcs"],
    "F3": ["NegN.2", "Negative control (AMHR2, no uAb) (2)", "Copy of AMHR2 2.fcs"],
    "C4": ["NegI.1", "Negative control (AMHR2, inert uAb) (1)", "Copy of Gen uAb_AMHR2 1.fcs"],
    "D4": ["NegI.2", "Negative control (AMHR2, inert uAb) (2)", "Copy of Gen uAb_AMHR2 2.fcs"],
    "E4": ["IuAb.1", "Inert uAb (1)", "Copy of General uAb 1.fcs"],
    "F4": ["IuAb.2", "Inert uAb (2)", "Copy of General uAb 2.fcs"],
}

WT_WELL = "A1"

plate = FCPlate(
    "final_project_plate", {
        k: FCMeasurement(ID=v[0], datafile=ROOT+v[2])
        for k, v in plate_info.items()
    },
    "name", shape=(6, 4)
)


##

if False:
    all_events: FCMeasurement = plate["B4"]
    viable:     FCMeasurement = all_events.gate(viable_gate)
    singlets:   FCMeasurement = viable.gate(singlets_gate)

    plt.clf()
    x1 = singlets.get_data()["mCherry-H"]
    y1 = singlets.get_data()["GFP-H"]

    x2 = [xk for xk, yk in zip(x1, y1) if xk > 0 and yk > 0]
    y2 = [yk for xk, yk in zip(x1, y1) if xk > 0 and yk > 0]

    plt.scatter(x2, y2, s=2, lw=0, c=heatmapify(x2, y2, 100, 100))
    plt.loglog()
    plt.xlim(0.5e0, 1e6)
    plt.ylim(0.5e0, 1e6)
    plt.show()

    import sys
    sys.exit()

##

if False:

    all_events: FCMeasurement = plate["A1"].gate(viable_gate).get_data()

    # plate["A1"].view_interactively()

    GFPH = all_events["GFP-H"]
    FSCH = all_events["FSC-H"]
    FSCA = all_events["FSC-A"]
    SSCH = all_events["SSC-H"]

    FSCH_nz = [f for g, s, f, a in zip(GFPH, SSCH, FSCH, FSCA) 
        if g > 0 and s > 0 and f > 0 and f < 0.9e6 and s < 0.9e6 and a > 0]
    GFPH_nz = [g for g, s, f, a in zip(GFPH, SSCH, FSCH, FSCA) 
        if g > 0 and s > 0 and f > 0 and f < 0.9e6 and s < 0.9e6 and a > 0]
    SSCH_nz = [s for g, s, f, a in zip(GFPH, SSCH, FSCH, FSCA) 
        if g > 0 and s > 0 and f > 0 and f < 0.9e6 and s < 0.9e6 and a > 0]
    FSCA_nz = [a for g, s, f, a in zip(GFPH, SSCH, FSCH, FSCA) 
        if g > 0 and s > 0 and f > 0 and f < 0.9e6 and s < 0.9e6 and a > 0]
    heat = heatmapify(FSCA_nz, FSCH_nz, 100, 100, False)

    vx = [v[0] for v in viable_gate.vert]
    vy = [v[1] for v in viable_gate.vert]

    sx = [s[0] for s in singlets_gate.vert]
    sy = [s[1] for s in singlets_gate.vert]

    vx = vx + [vx[0]]
    vy = vy + [vy[0]]
    sx = sx + [sx[0]]
    sy = sy + [sy[0]]

    plt.scatter(FSCA_nz, FSCH_nz, s=4, lw=0, c=np.log10(heat), cmap="viridis")
    plt.xlabel("FSC-A")
    plt.ylabel("FSC-H")
    plt.plot(sx, sy, 'k-o')
    plt.title("Singlets Gate")
    plt.savefig("singlets.png", dpi=300)
    # plt.xlim(5e4, 1e6)
    # plt.ylim(1e2, 1e6)
    # plt.loglog() 
    # plt.show()

    import sys
    sys.exit()

##

def qprint(m):
    if False:
        print(m)

for target in ["GFP-H"]:

    shade_color = 'g' if target == "GFP-H" else 'r'

    expt_layout = (2, 7)
    ctrl_layout = (2, 5)

    expt_wells = list(plate_info.keys())[:14]
    ctrl_wells = list(plate_info.keys())[14:]

    factor = 1.3
    expt_size = (35 * factor, 10 * factor)
    ctrl_size = (25 * factor, 10 * factor)

    well_set = [
        ["expt", expt_wells, expt_layout, expt_size], 
        ["ctrl", ctrl_wells, ctrl_layout, ctrl_size]
    ]

    for group, wells, layout, size in well_set:

        fig, axs = plt.subplots(layout[0], layout[1], figsize=size)

        for i in range(len(axs.flat) - len(wells)):
            axs.flat[-(i+1)].set_axis_off()

        for i, well in enumerate(wells):

            row = i % layout[0]
            col = int((i - row) // layout[0])

            ax: plt.Axes = axs[row, col]

            all_events: FCMeasurement = plate[well]
            viable:     FCMeasurement = all_events.gate(viable_gate)
            singlets:   FCMeasurement = viable.gate(singlets_gate)

            num_events = len(all_events.get_data())
            num_viable = len(viable.get_data())
            num_singlets = len(singlets.get_data())

            qprint(f" ~~ ({i + 1 + (14 if group == 'ctrl' else 0)}/24) ~~")
            qprint(f"{num_viable/num_events*100:5.2f}% of all events are viable"
                + f" ({num_viable} / {num_events})")
            qprint(f"{num_singlets/num_viable*100:5.2f}% of viable are singlets"
                + f" ({num_singlets} / {num_viable})")

            tgtH = singlets.get_data()[target]
            SSCH = singlets.get_data()["SSC-H"]
            MCH = singlets.get_data()["mCherry-H"]

            tgtH_nz = [g for g, s, m in zip(tgtH, SSCH, MCH) if g > 0 and s > 0 and m > 1.5e2]
            SSCH_nz = [s for g, s, m in zip(tgtH, SSCH, MCH) if g > 0 and s > 0 and m > 1.5e2]

            qprint(f"{len(tgtH) - len(tgtH_nz)} negative/nonzero removed")

            tgt_positive = len([v for v in tgtH_nz if v > 1.5e2])
            tgt_pos_perc = tgt_positive/len(tgtH_nz)*100
            qprint(f"{tgt_pos_perc:5.2f}% {target[:-2]}+")
            print(f"{plate_info[well][0]} / {target[:-2]}+: " + 
                f"{tgt_pos_perc:6.3f}% of mCherry+")

            color = heatmapify(tgtH_nz, SSCH_nz, 100, 100)

            ax.scatter(tgtH_nz, SSCH_nz, s=4, lw=0, c=color, cmap="viridis")
            ax.add_collection(PatchCollection(
                [Rectangle((1.5e2, 0), 1e6, 5e5)], facecolor=shade_color, 
                alpha=0.15))
            ax.vlines(1.5e2, 0, 5e5, color=shade_color)
            ax.loglog()
            ax.set_xlim(0.5e0, 1e6)
            ax.set_ylim(5.7e4, 4.2e5)
            ax.set_xlabel(target)
            ax.set_ylabel("SSC-H")
            ax.set_title(f"{plate_info[well][0]} / {target[:-2]}+: " + 
                f"{tgt_pos_perc:6.3f}% of mCherry+", fontweight="bold")

        fname = f"{target}-{group}.png"
        print(f"Saving {fname} ...")
        plt.savefig("new_" + fname, dpi=500)
        plt.savefig("new_" +"LQ_" + fname, dpi=75)
        print(f"Done saving {fname}.")
