from FlowCytometryTools import FCPlate, FCMeasurement
from FlowCytometryTools import PolyGate, ThresholdGate

viable_gate = PolyGate([
        (3.823e+05, 2.786e+05), (2.635e+05, 2.077e+05), 
        (1.991e+05, 1.481e+05), (2.132e+05, 8.284e+04), 
        (3.259e+05, 5.731e+04), (4.688e+05, 8.568e+04), 
        (6.299e+05, 1.509e+05), (8.191e+05, 2.446e+05), 
        (8.634e+05, 3.155e+05), (8.271e+05, 3.949e+05), 
        (7.446e+05, 4.091e+05), (6.178e+05, 3.694e+05), 
        (5.010e+05, 3.325e+05)
    ], ('FSC-H', 'SSC-H'), region='in', name='viable_gate')

singlets_gate = PolyGate([
        (1.070e+05, 2.194e+05), (8.556e+05, 8.387e+05), 
        (1.006e+06, 8.208e+05), (2.979e+05, 2.122e+05)
    ], ('FSC-A', 'FSC-H'), region='in', name='singlets_gate')

mCherry_gate = ThresholdGate(1.5e2, "mCherry-H", "above")
GFP_nz_gate = ThresholdGate(0.01, "GFP-H", "above")
GFP_thr_gate = ThresholdGate(1.5e2, "GFP-H", "above")

import matplotlib.pyplot as plt
import numpy as np
from statistics import median

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = ["Times New Roman"]

ROOT = "data-from-lauren/"

plate_info = {
    "A1": ["1.1", "Peptide 1 (1)", "2.1 1.fcs"],
    "B1": ["1.2", "Peptide 1 (2)", "2.1 2.fcs"],
    "A2": ["4.1", "Peptide 4 (1)", "2.4 1.fcs"],
    "B2": ["4.2", "Peptide 4 (2)", "2.4 2.fcs"],
    "A3": ["6.1", "Peptide 6 (1)", "2.6 1.fcs"],
    "B3": ["6.2", "Peptide 6 (2)", "2.6 2.fcs"],
    "A4": ["8.1", "Peptide 8 (1)", "2.8 1.fcs"],
    "B4": ["8.2", "Peptide 8 (2)", "2.8 2.fcs"],
    "C1": ["10.1", "Peptide 10 (1)", "2.10 1.fcs"],
    "D1": ["10.2", "Peptide 10 (2)", "2.10 2.fcs"],
    "C2": ["11.1", "Peptide 11 (1)", "2.11 1.fcs"],
    "D2": ["11.2", "Peptide 11 (2)", "2.11 2.fcs"],
    "C3": ["12.1", "Peptide 12 (1)", "2.12 1.fcs"],
    "D3": ["12.2", "Peptide 12 (2)", "2.12 2.fcs"],
    "E1": ["Blank.1", "Blank (no AMHR2, no uAb) (1)", "Copy of Blank 3.fcs"],
    "F1": ["Blank.2", "Blank (no AMHR2, no uAb) (2)", "Copy of Blank 4.fcs"],
    "E2": ["Pos.1", "Positive control (AMHR2, uAb) (1)", "Copy of AMH uAb_AMHR2 1.fcs"],
    "F2": ["Pos.2", "Positive control (AMHR2, uAb) (2)", "Copy of AMH uAb_AMHR2 2.fcs"],
    "E3": ["NegN.1", "Negative control (AMHR2, no uAb) (1)", "Copy of AMHR2 2.fcs"],
    "F3": ["NegN.2", "Negative control (AMHR2, no uAb) (2)", "Copy of AMHR2 2.fcs"],
    "C4": ["NegI.1", "Negative control (AMHR2, inert uAb) (1)", "Copy of Gen uAb_AMHR2 1.fcs"],
    "D4": ["NegI.2", "Negative control (AMHR2, inert uAb) (2)", "Copy of Gen uAb_AMHR2 2.fcs"],
    "E4": ["IuAb.1", "Inert uAb (1)", "Copy of General uAb 1.fcs"],
    "F4": ["IuAb.2", "Inert uAb (2)", "Copy of General uAb 2.fcs"],
}

WT_WELL = "A1"

plate = FCPlate(
    "final_project_plate", {
        k: FCMeasurement(ID=v[0], datafile=ROOT+v[2])
        for k, v in plate_info.items()
    },
    "name", shape=(6, 4)
)

def qprint(m):
    if False:
        print(m)

counter = 0

mfis = {}

control_data = []

for well in (
        list(plate_info.keys())[20:22] + # negative control
        list(plate_info.keys())[:14]     # experimental
    ):

    all_events: FCMeasurement = plate[well]
    viable:     FCMeasurement = all_events.gate(viable_gate)
    singlets:   FCMeasurement = viable.gate(singlets_gate)
    mCherry:    FCMeasurement = singlets.gate(mCherry_gate)
    GFP_nz:     FCMeasurement = mCherry.gate(GFP_nz_gate)
    GFP_thr:    FCMeasurement = mCherry.gate(GFP_thr_gate)


    mCherry_data = mCherry.get_data()["mCherry-H"]
    GFP_nz_data = GFP_nz.get_data()["GFP-H"]
    GFP_thr_data = GFP_thr.get_data()["GFP-H"]

    MFI = median(GFP_nz_data)

    mfis[plate_info[well][0]] = MFI

    info = (f"[{plate_info[well][0]:>6s}] eGFP+: " + 
        f"{len(GFP_thr_data)/len(mCherry_data)*100:6.3f}% of mCherry+," +
        f" MFI = {MFI}")

    print(info)

    if counter < 2:
        control_data.extend(GFP_nz_data)
    else:
        plt.hist(np.log10(control_data), bins=50, alpha=0.5, color="b", 
            density=True, label="Neg. control")

    plt.hist(np.log10(GFP_nz_data), bins=50, alpha=0.5, color=("g" 
        if counter >= 2 else "b"), density=True, label=plate_info[well][0])
    plt.title(info, fontweight="bold")
    if counter >= 2:
        plt.legend()
    plt.xlim((1, 4))
    plt.xlabel("log10(GFP-H)")
    plt.ylabel("Normalized Count (density)")

    fname = f"hist3/{plate_info[well][0]}_GFP-H.png"
    qprint(f"Saving {fname} ...")
    plt.savefig(fname, dpi=500)
    qprint(f"Done saving {fname}.")
    plt.clf()

    counter += 1

# print(mfis)

print("\nMFI information:\n")

for k in ["1", "4", "6", "8", "10", "11", "12", "NegI"]:
    rep1mfi = mfis[k+'.1']
    rep2mfi = mfis[k+'.2']
    print(f"{k}\t{rep1mfi}\t{rep2mfi}")
